��          �      l      �     �     �               ,     F     b     |     �     �     �  .   �     �     �          !     3     D  @   Y     �  �  �     l     �     �     �  .   �  0   �  $   0     U     g     �     �  <   �  
   �      �          "     3     F  :   ]     �     
                                           	                                                  Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Left hand layout May require logout/login 
to see the changes.
 Mouse Acceleration Mouse Acceleration Reset Mouse Options Right hand layout Size (in pixels) Successfully updated There is an error,
please rerun and correct the following error! Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 17:30+0300
Last-Translator: David Rebolo Magariños <drgaga345@gmail.com>
Language-Team: Galician (Spain) (http://www.transifex.com/anticapitalista/antix-development/language/gl_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gl_ES
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Velocidade (multiplicador) Todas as opcións establecidas Orde dos botóns Cambiar o tema do cursor Non foi posible executar o comando ds-mouse -a Non foi posible executar o comando ds-mouse -all Non foi posible executar ds-mouse -s Tamaño do cursor Restablecer o tamaño do cursor Tema do cursor Para man esquerda Pode ser necesario reiniciar a sesión
para ver os cambios.
 Velocidade Restablecer a velocidade do rato Opcións do rato Para man dereita Tamaño (píxeles) Actualizado con éxito Hai un erro.
Volver a executar e corrixir o seguinte erro: Límite (Píxeles) 