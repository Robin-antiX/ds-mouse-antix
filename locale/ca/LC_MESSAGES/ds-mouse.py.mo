��          �      �           	     #     3     @  D   T  C   �     �     �          -     9     K  1   X  -   �     �     �     �  (   �  ,        H     Y     a  �  t       "   2     U     g  S   �  P   �     &      E     f     �  #   �     �  ;   �  ?        F  +   _     �  5   �  9   �     	     !	     *	                       
                                                       	                          Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Size (in pixels) Startup Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 17:26+0300
Last-Translator: Eduard Selma <selma@tinet.cat>
Language-Team: Catalan (http://www.transifex.com/anticapitalista/antix-development/language/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Acceleració (Multiplicador) S'han establert totes les opcions  Ordre dels botons Canvia el tema del cursor No s'ha pogut desactivar.
Si us plau, editeu ~/.desktop-session/startup manualment. No s'ha pogut activar.
Si us plau, editeu ~/.desktop-session/startup manualment. No es pot executar ds-mouse -a No es pot executar ds-mouse-all  No es pot executar ds-mouse -s  Mida del Cursor S'ha restablert la mida del cursor  Tema del Cursor Activa o desactiva la configuració del ratolí en engegar
 Pot ser necessari desconnectar/connectar 
per veure els canvis. Acceleració del ratolí S'ha restablert l'acceleració del ratolí  Opcions del ratolí La configuració del ratolí es carregarà en engegar La configuració del ratolí no es carregarà en arrencar Mida (en píxels) Engegada Llindar (píxels) 