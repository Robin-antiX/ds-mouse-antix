��          �      �           	     #     3     @  D   T  C   �     �     �          -     9     K  1   X  -   �     �     �     �  (   �  ,        H     Y     a  �  t          8     X     e  O   w  N   �  !        8     W     t      �     �  8   �  ;   �     '      8     Y  (   m  -   �     �     �     �                       
                                                       	                          Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Size (in pixels) Startup Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 17:33+0300
Last-Translator: Henry Oquist <henryoquist@nomalm.se>
Language-Team: Swedish (http://www.transifex.com/anticapitalista/antix-development/language/sv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Acceleration (Multiplikator) Alla Valmöjligheter Inställda Knappordning Byt muspekar-tema Kunde inte avaktivera.
Var vänlig redigera ~/.desktop-session/startup manuellt Kunde inte aktivera. 
Var vänlig redigera ~/.desktop-session/startup manuellt Kunde inte köra ds-mouse -all -a Kunde inte köra ds-mouse -all Kunde inte köra ds-mouse -s Markörstorlek Muspekar-storlek Återställning Muspekar-tema Aktivera eller Avaktivera mus-konfiguration med startup
 Kan behöva utloggning/inloggning
för att se ändringarna. Mus-acceleration Mus-acceleration Återställning Mus Valmöjligheter Mus-konfiguration sätts på med startup Mus-konfiguration sätts inte på med startup Storlek (i pixels) Startup Tröskel (Pixels) 