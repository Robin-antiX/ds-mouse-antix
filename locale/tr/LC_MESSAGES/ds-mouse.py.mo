��          �      �           	     #     3     @  D   T  C   �     �     �          -     9     K  1   X  -   �     �     �     �  (   �  ,        H     Y     a  �  t           4     L     ]  W   {  M   �  !   !  %   C  !   i     �     �     �  U   �  C        a     n     �  2   �  5   �     	     	     '	                       
                                                       	                          Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Size (in pixels) Startup Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-05-03 18:10+0300
Last-Translator: mahmut özcan <mahmutozcan@protonmail.com>
Language-Team: Turkish (http://www.transifex.com/anticapitalista/antix-development/language/tr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tr
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.3
 İvme (Artırıcı) Tüm Seçenekler Ayarı Düğme Sırası İmleç temasını değiştir Devre dışı bırakılamadı.
 Lütfen ~/.desktop-session/startup'ı elle düzenleyin  Etkinleştirilemedi.
 Lütfen ~/.desktop-session/startup'ı elle düzenleyin  ds-mouse çalıştırılamadı -a ds-mouse çalıştırılamadı -hepsi ds-mouse çalıştırılamadı -s İmleç Boyutu İmleç Boyutunu Sıfırlama İmleç Teması Başlangıçta fare yapılandırmasını etkinleştirin veya devre dışı bırakın
 Değişiklikleri görmek için
oturumu kapatıp açmak gerekebilir. Fare İvmesi Fare İvmesini Sıfırlama Fare Seçenekleri Fare yapılandırması başlangıçta yüklenecek  Fare yapılandırması başlangıçta yüklenmeyecek  Boyut (piksel olarak) Başlangıç Eşik (Piksel) 