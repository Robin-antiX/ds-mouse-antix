��          �      �           	     #     3     @  D   T  C   �     �     �          -     9     K  1   X  -   �     �     �     �  (   �  ,        H     Y     a  �  t          <     [     k  O   �  P   �  "   $  $   G  "   l     �     �     �  9   �  6        :     K     f  1   s  5   �     �     �     �                       
                                                       	                          Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Size (in pixels) Startup Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 17:33+0300
Last-Translator: Besnik Bleta <besnik@programeshqip.org>
Language-Team: Albanian (http://www.transifex.com/anticapitalista/antix-development/language/sq/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sq
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Përshpejtim (Shumëfishues) Krejt Mundësitë të Ujdisura Radhë Butonash Ndryshoni temë kursori S’u aktivizua dot.
 Ju lutemi, përpunojeni ~/.desktop-session/startup dorazi S’u aktivizua dot. 
 Ju lutemi, përpunojeni ~/.desktop-session/startup dorazi S’u xhirua dot “ds-mouse -a” S’u xhirua dot “ds-mouse -all” S’u xhirua dot “ds-mouse -s” Madhësi Kursori Ricaktim Madhësie Kursori Temë Kursori Aktivizoni ose Çaktivizoni formësim miu gjatë nisjesh
 Mund të lypë dalje/hyrje 
që të shihni ndryshimet. Përshpejtim Miu Ricaktim Përshpejtimi Miu Mundësi Miu Formësimi i miut do të ngarkohet gjatë nisjesh Formësimi i miut s’do të ngarkohet gjatë nisjesh Madhësi (në piksel) Nisje Prag (Piksel) 