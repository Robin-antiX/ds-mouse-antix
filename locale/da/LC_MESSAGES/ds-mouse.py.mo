��          �      l      �     �     �               ,     F     b     |     �     �     �  .   �     �     �          !     3     D  @   Y     �  �  �     <     Y     x     �     �     �     �     �          3     C  9   Y     �     �     �     �     �       =        W     
                                           	                                                  Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Left hand layout May require logout/login 
to see the changes.
 Mouse Acceleration Mouse Acceleration Reset Mouse Options Right hand layout Size (in pixels) Successfully updated There is an error,
please rerun and correct the following error! Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 17:26+0300
Last-Translator: scootergrisen
Language-Team: Danish (http://www.transifex.com/anticapitalista/antix-development/language/da/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: da
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Acceleration (multiplikator) Alle valgmuligheder indstillet Knappernes rækkefølge Skift markørens tema Kunne ikke køre ds-mouse -a Kunne ikke køre ds-mouse -all Kunne ikke køre ds-mouse -s Markørens størrelse Nulstil markørens størrelse Markørens tema Venstrehåndet layout Det kan kræve udlogning/indlogning 
at se ændringerne.
 Musens acceleration Nulstil musens acceleration Valgmuligheder for mus Højrehåndet layout Størrelse (i pixels) Opdatering lykkedes Der opstod en fejl,
kør venligst igen og ret følgende fejl! Tærskel (pixels) 