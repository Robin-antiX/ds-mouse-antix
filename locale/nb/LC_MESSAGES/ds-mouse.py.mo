��          �      �           	     #     3     @  D   T  C   �     �     �          -     9     K  1   X  -   �     �     �     �  (   �  ,        H     Y     a  �  t     !     >     V  	   h  D   r  E   �  "   �  $      "   E     h     x  	   �  0   �  5   �               5  (   F  -   o     �     �     �                       
                                                       	                          Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Size (in pixels) Startup Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 17:32+0300
Last-Translator: heskjestad <cato@heskjestad.xyz>
Language-Team: Norwegian Bokmål (http://www.transifex.com/anticapitalista/antix-development/language/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Akselerasjon (multiplikator) Alle alternativer valgt Knapperekkefølge Musepeker Klarte ikke slå av.
Rediger fila ~/.desktop-session/startup manuelt Klarte ikke slå på.
Rediger fila ~/.desktop-session/startup manuelt Klarte ikke kjøre «ds-mouse -a» Klarte ikke kjøre «ds-mouse -all» Klarte ikke kjøre «ds-mouse -s» Pekerstørrelse Tilbakestill pekerstørrelse Pekertema Slå av eller på oppsett av mus under oppstart
 Kan kreve inn- eller utlogging 
for å se endringene. Museakselerasjon Tilbakestill museakselerasjon Musealternativer Oppsett av mus vil lastes under oppstart Oppsett av mus vil ikke lastes under oppstart Størrelse (i piksler) Oppstart Terskel (piksler) 