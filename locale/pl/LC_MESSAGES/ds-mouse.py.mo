��          �      l      �     �     �               ,     F     b     |     �     �     �  .   �     �     �          !     3     D  @   Y     �  2  �     �     �          +  !   @  #   b  !   �     �     �     �     �  =   �     .     C     ^     o     �     �  D   �     �     
                                           	                                                  Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Left hand layout May require logout/login 
to see the changes.
 Mouse Acceleration Mouse Acceleration Reset Mouse Options Right hand layout Size (in pixels) Successfully updated There is an error,
please rerun and correct the following error! Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 17:32+0300
Last-Translator: Filip Bog <mxlinuxpl@gmail.com>
Language-Team: Polish (http://www.transifex.com/anticapitalista/antix-development/language/pl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pl
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
X-Generator: Poedit 2.3
 Przyspieszenie (mnożnik)  Zestaw wszystkich opcji Kolejność przycisków Zmień motyw kursora Nie można uruchomić ds-mouse -a Nie można uruchomić ds-mouse -all Nie można uruchomić ds-mouse -s Rozmiar kursora Reset rozmiaru kursora Motyw kursora Układ leworęczny Może wymagać wylogowania/zalogowania
aby zobaczyć zmiany.
 Przyspieszenie myszy Reset przyspieszenia myszy Ustawienia myszy Układ praworęczny Rozmiar (w pikselach) Zaktualizowano pomyślnie Pojawia się błąd,
uruchom ponownie i popraw następujący błąd! Próg (piksele)  