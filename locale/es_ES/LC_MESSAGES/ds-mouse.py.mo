��          �      �           	     #     3     @  D   T  C   �     �     �          -     9     K  1   X  -   �     �     �     �  (   �  ,        H     Y     a  �  t          /     O     d  F     C   �     
  !   *     L     l  '        �  @   �  H   �     A  -   X     �  1   �  3   �      	     	     	                       
                                                       	                          Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Size (in pixels) Startup Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 17:28+0300
Last-Translator: Casper casper
Language-Team: Spanish (Spain) (http://www.transifex.com/anticapitalista/antix-development/language/es_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es_ES
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Aceleración (Multiplicador) Todas las opciones establecidas Orden de los Botones Cambiar el tema del cursor No se pudo deshabilitar.
Edite ~/.sesion-escritorio/inicio manualmente No se pudo habilitar.
Edite ~/.sesion-escritorio/inicio manualmente No se pudo ejecutar ds-mouse -a No se pudo ejecutar ds-mouse -all No se pudo ejecutar ds-mouse -s Tamaño del Cursor Restablecimiento del tamaño del cursor Tema del Cursor Habilitar o deshabilitar la configuración del mouse al iniciar
 Puede requerir Cierre de sesión/Inicio de sesión
para ver los cambios. Aceleración del Mouse Restablecimiento de la aceleración del mouse Opciones del ratón La configuración del mouse se cargará al inicio La configuración del mouse no se cargara al inicio Tamaño (en pixeles) Iniciar Umbral (Pixeles) 