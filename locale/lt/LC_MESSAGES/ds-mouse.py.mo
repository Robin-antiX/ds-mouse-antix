��          �      l      �     �     �               ,     F     b     |     �     �     �  .   �     �     �          !     3     D  @   Y     �    �     �     �     �     �          1     Q     o          �     �  @   �               3     E     c     v  ?   �     �     
                                           	                                                  Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Left hand layout May require logout/login 
to see the changes.
 Mouse Acceleration Mouse Acceleration Reset Mouse Options Right hand layout Size (in pixels) Successfully updated There is an error,
please rerun and correct the following error! Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 17:32+0300
Last-Translator: Moo
Language-Team: Lithuanian (http://www.transifex.com/anticapitalista/antix-development/language/lt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lt
Plural-Forms: nplurals=4; plural=(n % 10 == 1 && (n % 100 > 19 || n % 100 < 11) ? 0 : (n % 10 >= 2 && n % 10 <=9) && (n % 100 > 19 || n % 100 < 11) ? 1 : n % 1 != 0 ? 2: 3);
X-Generator: Poedit 2.3
 Pagreitis (Koeficientas) Visi parametrai nustatyti Mygtukų tvarka Keisti žymeklio temą Nepavyko paleisti ds-mouse -a Nepavyko paleisti ds-mouse -all Nepavyko paleisti ds-mouse -s Žymeklio dydis Žymeklio dydis atstatytas Žymeklio tema Kairės rankos išdėstymas Norint pamatyti pakeitimus 
gali tekti atsijungti/prisijungti.

 Pelės pagreitis Pelės pagreitis atstatytas Pelės parametrai Dešinės rankos išdėstymas Dydis (pikseliais) Sėkmingai atnaujinta Yra klaida,
paleiskite iš naujo ir ištaisykite šią klaidą! Slenkstis (Pikseliais) 