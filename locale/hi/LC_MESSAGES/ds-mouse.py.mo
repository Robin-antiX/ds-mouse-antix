��          �      �           	     #     3     @  D   T  C   �     �     �          -     9     K  1   X  -   �     �     �     �  (   �  ,        H     Y     a  �  t       0   6     g  0   ~  �   �  �   N  1   �  3     1   J  #   |  3   �      �  z   �  x   p	     �	  3   
     :
  Z   Z
  g   �
  .        L  .   e                       
                                                       	                          Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Size (in pixels) Startup Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 17:30+0300
Last-Translator: Panwar108 <caspian7pena@gmail.com>
Language-Team: Hindi (http://www.transifex.com/anticapitalista/antix-development/language/hi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hi
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 त्वरण (गुणक) सभी विकल्प सेट हैं बटन क्रम कर्सर की थीम बदलें निष्क्रिय करना विफल। 
कृपया ~/.desktop-session/startup को स्वयं संपादित करें सक्रिय करना विफल। 
कृपया ~/.desktop-session/startup को स्वयं संपादित करें ds-mouse -a निष्पादन विफल ds-mouse -all निष्पादन विफल ds-mouse -s निष्पादन विफल कर्सर का आकार कर्सर आकार पुनः सेट कर्सर की थीम आरंभ होने पर माउस विन्यास सक्रिय या निष्क्रिय
 परिवर्तन लागू करने हेतु
लॉगआउट/लॉगिन आवश्यक। माउस त्वरण माउस त्वरण पुनः सेट माउस विकल्प माउस विन्यास आरंभ होने पर लोड होगा माउस विन्यास आरंभ होने पर लोड नहीं होगा आकार (पिक्सेल में) आरंभिकरण सीमा (पिक्सेल में) 