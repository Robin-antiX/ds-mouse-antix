��          �      �           	     #     3     @  D   T  C   �     �     �          -     9     K  1   X  -   �     �     �     �  (   �  ,        H     Y     a  �  t       )   ;     e     w  U   �  Q   �  (   /  (   X  (   �     �  ,   �     �  C   �  N   ?     �  5   �     �  9   �  @   /	     p	  
   �	     �	                       
                                                       	                          Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Size (in pixels) Startup Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 17:30+0300
Last-Translator: cyril cottet <cyrilusber2001@yahoo.fr>
Language-Team: French (http://www.transifex.com/anticapitalista/antix-development/language/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.3
 Accélération (multiplicateur) Toutes les options ont été configurées Ordre des boutons Choix du thème Désactivation impossible. 
 Veuillez éditer manuellement ~/.desktop-session/startup Activation impossible. 
 Veuillez éditer manuellement ~/.desktop-session/startup ds-mouse -a n'a pas pu être exécuté  ds-mouse -all n'a pas pu être exécuté ds-mouse -s n'a pas pu être exécuté  Taille du curseur La taille du curseur a été réinitialisée Thème du curseur Activer ou désactiver la configuration de la souris au démarrage
 Peut nécessiter de fermer puis rouvrir la session 
pour voir les changements. Accélération de la souris L'accélération de la souris a été réinitialisée Options de la souris La configuration de la souris sera chargée au démarrage La configuration de la souris ne sera pas chargée au démarrage Taille (en pixels) Démarrage Seuil (Pixels) 