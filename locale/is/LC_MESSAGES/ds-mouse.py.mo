��          �      l      �     �     �               ,     F     b     |     �     �     �  .   �     �     �          !     3     D  @   Y     �  �  �  &   i     �     �     �  !   �  #   �  !        4      D     e     q  B   �     �      �     �               -  F   A     �     
                                           	                                                  Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Left hand layout May require logout/login 
to see the changes.
 Mouse Acceleration Mouse Acceleration Reset Mouse Options Right hand layout Size (in pixels) Successfully updated There is an error,
please rerun and correct the following error! Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 17:30+0300
Last-Translator: Sveinn í Felli <sv1@fellsnet.is>
Language-Team: Icelandic (http://www.transifex.com/anticapitalista/antix-development/language/is/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: is
Plural-Forms: nplurals=2; plural=(n % 10 != 1 || n % 100 == 11);
X-Generator: Poedit 2.3
 Músarhröðun (margföldunarstuðull) Allir valkostir stilltir Röð hnappa Skipta um bendilþema Ekki tókst að keyra ds-mouse -a Ekki tókst að keyra ds-mouse -all Ekki tókst að keyra ds-mouse -s Stærð bendils Endurstilling á stærð bendils Bendilþema Örvhent snið Gæti þurft að skrá sig út/inn 
til að breytingar komi fram.
 Hröðun músar Endurstilling á hröðun músar Valkostir músar Rétthent snið Stærð (í mynddílum) Tókst að uppfæra Upp kom villa,
keyrðu þetta aftur og leiðréttu eftirfarandi villu! Þolvik (mynddílar) 