��          �      l      �     �     �               ,     F     b     |     �     �     �  .   �     �     �          !     3     D  @   Y     �  �  �     W     s     �     �     �     �     �          !     9     J  ]   b     �     �     �     �          %  F   ?     �     
                                           	                                                  Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Left hand layout May require logout/login 
to see the changes.
 Mouse Acceleration Mouse Acceleration Reset Mouse Options Right hand layout Size (in pixels) Successfully updated There is an error,
please rerun and correct the following error! Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 17:29+0300
Last-Translator: Oi Suomi On! <oisuomion@protonmail.com>
Language-Team: Finnish (http://www.transifex.com/anticapitalista/antix-development/language/fi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Kiihdytys (moninkertaistus) Kaikki vaihtoehdot asetettu  Painikkeiden järjestys Muuta kohdistimen teemaa ds-mouse -a ei voitu ajaa ds-mouse -all ei voitu ajaa ds-mouse -s ei voitu ajaa Osoittimen koko Palauta osoittimen koko Osoittimen teema Vasemmankäden asettelu Saattaa vaatia uloskirjautumisen/sisäänkirjautumisen 
jotta muutokset saadaan näkyväksi.
 Hiiren kiihtyvyys Nollaa hiiren kiihtyvyys Hiiren asetukset Oikeankäden asettelu Koko (pikseleinä) Päivitetty onnistuneesti Havaittiin virhe,
Suorita toiminto uudelleen ja korjaa seuraava virhe! Kynnysarvo (pikselit) 