��          �      �           	     #     3     @  D   T  C   �     �     �          -     9     K  1   X  -   �     �     �     �  (   �  ,        H     Y     a  �  t               0     E  I   [  H   �     �           .     M     \     s  <   �  ;   �     �     
     #  0   /  5   `     �  	   �     �                       
                                                       	                          Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Size (in pixels) Startup Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 17:32+0300
Last-Translator: Eadwine Rose
Language-Team: Dutch (http://www.transifex.com/anticapitalista/antix-development/language/nl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Acceleratie (Multiplier) Alle Opties Ingesteld Volgorde van knoppen Verander Cursor-Thema Kon niet uitschakelen. 
 Pas aub ~/.desktop-session/startup handmatig aan Kon niet inschakelen. 
 Pas aub ~/.desktop-session/startup handmatig aan Kon ds-mouse -a niet uitvoeren Kon ds-mouse -all niet uitvoeren Kon ds-mouse -s niet uitvoeren Cursor-grootte Cursor Grootte Herstel Cursor-thema Muisconfiguratie tijdens het opstarten aan- of uitschakelen
 Eventueel logout/login vereist
om de veranderingen te zien. Muis-acceleratie Muis Acceleratie Herstel Muis-opties Muisconfiguratie zal laden tijdens het opstarten Muisconfiguratie zal niet laden tijdens het opstarten Grootte (in pixels) Opstarten Drempelwaarde (Pixels) 