��          �      l      �     �     �               ,     F     b     |     �     �  1   �  -   �               3  (   A  ,   j     �     �     �  �  �  (   l  $   �     �     �  $   �  &     $   8     ]  $   s     �  =   �  d   �  '   Q  6   y     �  9   �  9        =     \     c     
                                           	                                                  Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Size (in pixels) Startup Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 17:32+0300
Last-Translator: anticapitalista <anticapitalista@riseup.net>
Language-Team: Japanese (http://www.transifex.com/anticapitalista/antix-development/language/ja/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ja
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 2.3
 アクセラレーション (Multiplier) 全てのオプションをセット ボタンの順番 カーソルテーマの変更 ds-mouse -a を開始できません ds-mouse -all を開始できません ds-mouse -s を開始できません カーソルサイズ カーソルサイズのリセット カーソルテーマ 起動時にマウスの設定を有効／無効にします
 変更を確認するには、ログアウト/ログインが必要となる場合があります。 マウスのアクセラレーション マウスのアクセラレーションのリセット マウスオプション マウスの設定は起動時に読み込まれません マウスの設定は起動時に読み込まれません サイズ (ピクセル表示) 起動 しきい値 (ピクセル) 