��          �      l      �     �     �               ,     F     b     |     �     �     �  .   �     �     �          !     3     D  @   Y     �  �  �     �  (   �     �       $     &   B  #   i     �     �     �     �  L   �     +     A     Z     s     �     �  F   �     �     
                                           	                                                  Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Left hand layout May require logout/login 
to see the changes.
 Mouse Acceleration Mouse Acceleration Reset Mouse Options Right hand layout Size (in pixels) Successfully updated There is an error,
please rerun and correct the following error! Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 17:29+0300
Last-Translator: sonny nunag <sonnynunag@yahoo.com>
Language-Team: Filipino (Philippines) (http://www.transifex.com/anticapitalista/antix-development/language/fil_PH/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fil_PH
Plural-Forms: nplurals=2; plural=(n == 1 || n==2 || n==3) || (n % 10 != 4 || n % 10 != 6 || n % 10 != 9);
X-Generator: Poedit 2.3
 Acceleration (Multiplier) Ang lahat ng pagpipilian ay nakalapat na Kaayusan ng mga boton Baguhin ang tema ng cursor  Di maaring paandarin ang ds-mouse -a Di maaring paandarin ang ds-mouse -all Di kayang paandarin ang ds-mouse -s Laki ng Cursor I-reset ang laki ng cursor Tema ng Cursor  Latag na pang-kaliwete Maaring mangangailangan ng paglabas/pagpasok
upang makita ang mga pagbabago
 Acceleration ng Mouse Mouse Acceleration Reset Mga pagpipilian sa Mouse Latag na pang-kanan Laki (sa pixels) Matagumpay na naiangkop Ito ay isang error,
magsimula muli at itama ang mga sumusunod na mali! Threshold (Pixels) 