��          �      �           	     #     3     @  D   T  C   �     �     �          -     9     K  1   X  -   �     �     �     �  (   �  ,        H     Y     a  �  t  5   !  <   W     �  0   �  �   �  �   q  C   �  E   A  C   �     �  *   �     	  �   ,	  t   �	  '   -
  F   U
  #   �
  h   �
  o   )     �     �     �                       
                                                       	                          Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Size (in pixels) Startup Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 17:28+0300
Last-Translator: anticapitalista <anticapitalista@riseup.net>
Language-Team: Greek (http://www.transifex.com/anticapitalista/antix-development/language/el/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Επιτάχυνση (πολλαπλασιαστής) Όλες οι επιλογές έχουν ρυθμιστεί Σειρά κουμπιών Επιλέξτε Δείκτη Ποντικιού Δεν ήταν δυνατή η απενεργοποίηση
Επεξεργαστείτε μη αυτόματα το ~/.desktop-session/startup Δεν ήταν δυνατή η ενεργοποίηση
Επεξεργαστείτε μη αυτόματα το ~/.desktop-session/startup Δεν ήταν δυνατή η εκτέλεση του ds-mouse -a Δεν ήταν δυνατή η εκτέλεση του ds-mouse -all Δεν ήταν δυνατή η εκτέλεση του ds-mouse -s Δρομέας Μέγεθος Αλλαγή Δρομέας Μέγεθος Δρομέας Θέμα Ενεργοποίηση ή απενεργοποίηση της διαμόρφωσης ποντικιού κατά την εκκίνηση
 Παρακαλούμε αποσυνδεθείτε/συνδεθείτε
για να δείτε τις αλλαγές. Επιτάχυνση Ποντικιού Επιτάχυνση του ποντικιού έχει οριστεί Επιλογές Ποντικιού Η διαμόρφωση του ποντικιού θα φορτωθεί κατά την εκκίνηση Η διαμόρφωση του ποντικιού δεν θα φορτωθεί κατά την εκκίνηση Μέγεθος (σε pixels) Εκκίνηση Κατώφλι (Pixel) 