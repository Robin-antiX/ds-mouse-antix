��          �      l      �     �     �               ,     F     b     |     �     �     �  .   �     �     �          !     3     D  @   Y     �  �  �  
   �     �     �     �     �     �          3     E     b     q  :   �     �     �     �               3  I   M     �     
                                           	                                                  Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Left hand layout May require logout/login 
to see the changes.
 Mouse Acceleration Mouse Acceleration Reset Mouse Options Right hand layout Size (in pixels) Successfully updated There is an error,
please rerun and correct the following error! Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 17:26+0300
Last-Translator: joyinko <joyinko@azet.sk>
Language-Team: Czech (http://www.transifex.com/anticapitalista/antix-development/language/cs/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cs
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;
X-Generator: Poedit 2.3
 Akcelerace Všechny volby nastaveny Řád tlačítek  Změnit téma kurzoru Nelze spustit ds-mouse -a Nelze spustit ds-mouse -all Nelze spustit ds-mouse -s Velikost kurzoru  Zresetovat velikost kurzoru  Téma kurzoru  Rozvržení levé ruky  Prosím
odhlašte se/přihlašte
se pro projevení změn.
 Akcelerace myši  Zresetovat akceleraci myši  Možnosti myši  Rozvržení pravé ruky  Velikost (v pixelech) Uspěšně aktualizováno Nastala chyba,
prosím opakujte proces a odstraňte následující chybu! Limit (Pixely) 