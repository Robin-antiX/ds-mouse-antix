��          �      l      �     �     �               ,     F     b     |     �     �     �  .   �     �     �          !     3     D  @   Y     �  �  �     �     �     �     �     �          ,     H     Z     x     �  E   �     �     �          $     <     S  H   l     �     
                                           	                                                  Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Left hand layout May require logout/login 
to see the changes.
 Mouse Acceleration Mouse Acceleration Reset Mouse Options Right hand layout Size (in pixels) Successfully updated There is an error,
please rerun and correct the following error! Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 17:32+0300
Last-Translator: joyinko <joyinko@azet.sk>
Language-Team: Slovak (http://www.transifex.com/anticapitalista/antix-development/language/sk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sk
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n == 1 ? 0 : n % 1 == 0 && n >= 2 && n <= 4 ? 1 : n % 1 != 0 ? 2: 3);
X-Generator: Poedit 2.3
 Rýchlosť (Násobok) Všetky voľby boli nastavené Poradie tlačítok Zmeniť motív kurzoru Nešlo spustiť ds-mouse -a Nešlo spustiť ds-mouse -all Nešlo spustiť ds-mouse -s Veľkosť kurzoru Zresetovať veľkosť kurzoru Motív kurzoru Ovládanie ľavou rukou Pre zavedenie zmien sa môže 
vyžadovať odhlásenie/prihlásenie.
 Rýchlosť myši Zresetovať rýchlosť myši Nastavenia myši Ovládanie pravou rukou Veľkosť (v pixeloch) Úspešne aktualizované Vyskytla sa chyba, 
prosím spustite znovu a opravte nasledujúcu chybu! Prah (v pixeloch) 