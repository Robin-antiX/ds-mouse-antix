��          �      �           	     #     3     @  D   T  C   �     �     �          -     9     K  1   X  -   �     �     �     �  (   �  ,        H     Y     a  �  t     R     j     �     �  ?   �  >   �  #   +  %   O  #   u     �     �     �  /   �  9        B     S     q  0   �  3   �     �     �     	                       
                                                       	                          Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Size (in pixels) Startup Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 17:33+0300
Last-Translator: Arnold Marko <arnold.marko@gmail.com>
Language-Team: Slovenian (http://www.transifex.com/anticapitalista/antix-development/language/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
X-Generator: Poedit 2.3
 Pospešek (večkratnik) Nastavljene so vse opciije Zaporedje tipk Zamanjaj temo za kurzor Izklop ni bil mogoč.
Ročno uredite ~/.desktop-session/startup Vklop ni bil mogoč.
Ročno uredite ~/.desktop-session/startup Ni bilo mogoče zagnati ds-mouse -a Ni bilo mogoče zagnati ds-mouse -all Ni bilo mogoče zagnati ds-mouse -s Velikost kazalca Ponastavitev velikosti kazalca Tema za kurzor Vklopi ali izklopi nastavitve miške ob zagonu
 Da bodo spremembe vidne,
bo morda potreben ponovni zagon. Pospešek miške Ponastavitev pospeška miške Možnosti za miško Nastavitve za miško se bodo naložile ob zagonu Nastavitve za miško se ob zagonu ne bodo naložile Velikost (v pikslih) Zagon Prag (pikslov) 