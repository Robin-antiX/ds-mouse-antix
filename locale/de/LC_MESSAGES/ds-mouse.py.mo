��          �   %   �      `     a     {     �     �  D   �  C   �     5     O     k     �     �     �  1   �     �     �  -   �     '     :     S  (   a  ,   �     �     �     �     �     �  �  �     �     �     �     �  �   �  ~   g  %   �  '     %   4     Z     h     �  J   �     �     �  t   �     n	  !   �	     �	  ;   �	  A   �	     .
     @
     T
  "   `
     �
                                                                           	                      
                           Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 Error Left hand layout May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Right hand layout Size (in pixels) Startup Success Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-09-18 09:06+0200
Last-Translator: Robin
Language-Team: German (http://www.transifex.com/anticapitalista/antix-development/language/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Beschleunigungsfaktor Alle Einstellungen sind gesetzt Tastenordnung Ändern des Mauszeigerdesigns Deaktivieren nicht möglich. 
Bitte nehmen Sie die erforderlichen Einträge in der
Datei ~/.desktop-session/startup manuell vor. Einrichten nicht möglich. 
Bitte nehmen Sie die erforderlichen Einträge in
der Datei ~/.desktop-session/startup manuell vor. Konnte "ds-mouse -a" nicht ausführen Konnte "ds-mouse -all" nicht ausführen Konnte "ds-mouse -s" nicht ausführen Zeigergröße Zeigergröße zurückgesetzt Mauszeigerdesign Laden der Mauskonfiguration beim Startvorgang
aktivieren oder deaktivieren Fehler Linkshändermaus Es ist evtl. erforderlich, sich vom System
ab- und neu anzumelden, damit die
Änderungen des Designs wirksam werden. Mausbeschleunigung Mausbeschleunigung zurückgesetzt Mausoptionen Beim Systemstart wird die Konfiguration der Maus aktiviert. Beim Systemstart wird die Konfiguration der Maus nicht verwendet. Rechtshändermaus Größe (in Pixeln) Systemstart Die Anpassungen wurden vorgenommen Schwellenwert (in Pixeln) 