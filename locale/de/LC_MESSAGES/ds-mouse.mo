��    	      d      �       �   4   �   6     /   M     }     �     �     �  Q   �  �  &  [   �  V   +  V   �  %   �  $   �     $  j   D  �   �           	                                   -a   | set mouse motion (acceleration and threshold) -all | set mouse motion, button order, and cursor size -b   | set button order for left and right hand -h   | show this help dialog -s   | set cursor size Help: No option start settings gui out-of-range value for acceleration or threshold, so applying xset default values Project-Id-Version: ds-mouse version (...)
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2021-09-18 09:49+0200
Language-Team: https://www.transifex.com/anticapitalista/antix-development/dashboard/
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
Last-Translator: 
X-Generator: Poedit 2.3
X-Poedit-SourceCharset: UTF-8
   -a   | Mauszeigerreaktion definieren
         (Faktor und Schwellwert der Beschleunigung)   -all | Einrichten von Mausbeschleunigung,
         Tastenzuweisung und Zeigergröße   -b   | Tastenzuweisung für Rechtshänder- oder
         Linkshändermaus einstellen   -h   | Diese Programmhilfe anzeigen   -s   | Mauszeigergröße festlegen Hilfe:
  Befehlszeilenoptionen:   Wenn keine der Befehlszeilenoptionen angegeben ist,
  wird die graphische Benutzeroberfläche gestartet. Der Wert für Beschleunigung oder Aktivierungsschwelle liegt außerhalb der zulässigen Spanne. Daher wird er durch »xset« Standardwerte ersetzt. 