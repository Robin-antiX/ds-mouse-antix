��          �      �           	     #     3     @  D   T  C   �     �     �          -     9     K  1   X  -   �     �     �     �  (   �  ,        H     Y     a  �  t     0     M     i     {  `   �  ]   �  1   S  3   �  1   �     �     �       A   *  N   l     �  &   �     �  <   	  A   C	     �	     �	     �	                       
                                                       	                          Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Size (in pixels) Startup Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-05-03 18:09+0300
Last-Translator: felipe cardope <felipecardope@gmail.com>
Language-Team: Portuguese (Brazil) (http://www.transifex.com/anticapitalista/antix-development/language/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.3
 Aceleração (multiplicador) Todas as opções definidas Ordem dos Botões Alterar o tema do cursor Não foi possível desativar. 
Por favor edite manualmente o arquivo: ~/.desktop-session/startup Não foi possível ativar. 
Por favor edite manualmente o arquivo: ~/.desktop-session/startup Não foi possível executar o comando ds-mouse -a Não foi possível executar o comando ds-mouse -all Não foi possível executar o comando ds-mouse -s Tamanho do Cursor Redefinir o tamanho do cursor Tema do Cursor Ativar ou desativar a configuração do mouse na inicialização
 Pode exigir sair e entrar na
sessão (logout/login) para 
ver as alterações. Aceleração Redefinir a Aceleração do Rato/Mouse Opções do Rato/Mouse A configuração do mouse será carregada na inicialização A configuração do mouse não será carregada na inicialização Tamanho (pixel) Inicializar Limite (pixel) 