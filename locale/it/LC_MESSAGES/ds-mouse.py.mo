��          �      �           	     #     3     @  D   T  C   �     �     �          -     9     K  1   X  -   �     �     �     �  (   �  ,        H     Y     a  �  t           =     V     l  E   �  B   �  $     &   4  $   [     �     �     �  ;   �  ;        A  #   Y     }  4   �  8   �     �     	     	                       
                                                       	                          Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Size (in pixels) Startup Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 17:31+0300
Last-Translator: Davide Carli <dede.carli.drums@gmail.com>
Language-Team: Italian (http://www.transifex.com/anticapitalista/antix-development/language/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Accelerazione (esponenziale) Tutte le opzioni settate Ordinamento dei tasti Cambia tema del puntatore Non posso disabilitare. 
Edita ~/.desktop-session/startup manualmente Non posso abilitare. 
Edita ~/.desktop-session/startup manualmente Non è possibile avviare ds-mouse -a Non è possibile avviare ds-mouse -all Non è possibile avviare ds-mouse -s Dimensione del puntatore Reimposta Dimensione Cursore Tema del puntatore Abilita o disabilita la configurazione del mouse all'avvio
 Potrebbe richiedere logout/login 
per vedere i cambiamenti. Accelerazione del mouse Reimposta l'accelerazione del Mouse Opzioni del mouse  La configurazione del mouse sarà caricata all'avvio La configurazione del mouse non sarà caricata all'avvio Dimensione (in pixel) Avvio Limite (in pixel) 