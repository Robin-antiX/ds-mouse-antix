��          �      �           	     #     3     @  D   T  C   �     �     �          -     9     K  1   X  -   �     �     �     �  (   �  ,        H     Y     a  �  t          <     X     l  O   �  L   �  1   "  3   T  1   �     �      �     �  6   �  C   3     w  !   �     �  4   �  ?   �     -	     ?	     G	                       
                                                       	                          Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Size (in pixels) Startup Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 17:32+0300
Last-Translator: Paulo Carvalho <paulop1976@gmail.com>
Language-Team: Portuguese (http://www.transifex.com/anticapitalista/antix-development/language/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Aceleração (multiplicador) Todas as opções definidas Esquema dos Botões Alterar o tema do cursor Não consegui desativar.
Por favor edite manualmente ~/.desktop-session/startup Não consegui ativar.
Por favor edite manualmente ~/.desktop-session/startup Não foi possível executar o comando ds-mouse -a Não foi possível executar o comando ds-mouse -all Não foi possível executar o comando ds-mouse -s Tamanho do Cursor Restabelecer o tamanho do cursor Tema do Cursor Ativar ou desativar configuração do rato ao iniciar
 Poderá ser necessário fazer logout/lgin
para ver as alterações. Aceleração Restabelecer Aceleração do Rato Opções do rato  A configuração do Rato será carregada ao iniciar. A configuração do Rato não será carregada durante o início Tamanho (píxeis) Início Limite (pixel) 